(ns rover-clj.core
  (:gen-class
    :methods [[hello [String] String]]))

(defn -hello
  "I don't do a whole lot."
  [this x]
  (str "Hello, " x))
